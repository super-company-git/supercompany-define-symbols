using System;

[AttributeUsage(AttributeTargets.Assembly)]
public class AssemblyAddDefineAttribute : Attribute
{
    public string Value { get; private set; }

    public AssemblyAddDefineAttribute(string value) { Value = value; }
}
