using System;

[AttributeUsage(AttributeTargets.Assembly)]
public class AssemblyDefinePrefixAttribute : Attribute
{
    public string Value { get; private set; }

    public AssemblyDefinePrefixAttribute(string value) { Value = value; }
}
