using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Compilation;
using UnityEngine;

namespace Supercompany.Core.Editor
{
    public class Asmdef
    {
        public string[] includePlatforms;
        public string[] excludePlatforms;
    }

    public static class AddDefineSymbols
    {
        [InitializeOnLoadMethod]
        private static void OnProjectLoadedInEditor()
        {
            Dictionary<NamedBuildTarget, HashSet<string>> defineSymbolsToAddPerPlatform = new();
            HashSet<string> definePrefixes = new();

            System.Reflection.Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

            foreach (System.Reflection.Assembly assembly in assemblies)
            {
                object[] definePrefixesAttributes = assembly.GetCustomAttributes(typeof(AssemblyDefinePrefixAttribute), false);

                foreach (object attributeObject in definePrefixesAttributes)
                {
                    if (attributeObject is AssemblyDefinePrefixAttribute prefix)
                        definePrefixes.Add(prefix.Value);
                }

                object[] addDefineAttributes = assembly.GetCustomAttributes(typeof(AssemblyAddDefineAttribute), false);

                foreach (object attributeObject in addDefineAttributes)
                {
                    if (attributeObject is AssemblyAddDefineAttribute defineSymbol)
                    {
                        string asmdefPath = CompilationPipeline.GetAssemblyDefinitionFilePathFromAssemblyName(assembly.GetName().Name);
                        var asmdef = JsonUtility.FromJson<Asmdef>(File.ReadAllText(asmdefPath));

                        List<string> includePlatforms = new();

                        if ((asmdef.includePlatforms.Length == 0 && asmdef.excludePlatforms.Length == 0) || asmdef.excludePlatforms.Length > 0)
                        {
                            AssemblyDefinitionPlatform[] platforms = CompilationPipeline.GetAssemblyDefinitionPlatforms();

                            foreach (AssemblyDefinitionPlatform platform in platforms)
                                includePlatforms.Add(platform.Name);

                            foreach (string excludePlatform in asmdef.excludePlatforms)
                                includePlatforms.Remove(excludePlatform);
                        }
                        else
                        {
                            foreach (string includePlatform in asmdef.includePlatforms)
                                includePlatforms.Add(includePlatform);
                        }

                        foreach (string platform in includePlatforms)
                        {
                            NamedBuildTarget? buildTarget = GetBuildTargetFromPlatformName(platform);

                            if (buildTarget.HasValue)
                            {
                                if (!defineSymbolsToAddPerPlatform.ContainsKey(buildTarget.Value))
                                    defineSymbolsToAddPerPlatform[buildTarget.Value] = new HashSet<string>();

                                defineSymbolsToAddPerPlatform[buildTarget.Value].Add(defineSymbol.Value);
                            }
                        }
                    }
                }
            }

            foreach (NamedBuildTarget buildTarget in AllBuildTargets())
            {
                List<string> definesToAdd = new();
                List<string> definesToRemove = new();

                PlayerSettings.GetScriptingDefineSymbols(buildTarget, out string[] currentDefinesArray);
                List<string> currentDefines = currentDefinesArray.ToList();

                if (defineSymbolsToAddPerPlatform.TryGetValue(buildTarget, out HashSet<string> newDefines))
                {
                    foreach (string define in newDefines)
                    {
                        if (!currentDefines.Contains(define))
                            definesToAdd.Add(define);
                    }

                    foreach (string prefix in definePrefixes)
                        definesToRemove.AddRange(currentDefines.Where(x => x.StartsWith(prefix) && !newDefines.Contains(x)));
                }
                else
                {
                    foreach (string prefix in definePrefixes)
                        definesToRemove.AddRange(currentDefines.Where(x => x.StartsWith(prefix)));
                }

                if (definesToAdd.Count > 0 || definesToRemove.Count > 0)
                {
                    foreach (string define in definesToAdd)
                    {
                        if (!currentDefines.Contains(define))
                            currentDefines.Add(define);
                    }

                    foreach (string define in definesToRemove)
                        currentDefines.Remove(define);

                    PlayerSettings.SetScriptingDefineSymbols(buildTarget, currentDefines.ToArray());
                }
            }
        }

        private static NamedBuildTarget? GetBuildTargetFromPlatformName(string platform)
        {
            if (platform == "WindowsStandalone64" || platform == "WindowsStandalone32" || platform == "macOSStandalone" || platform == "LinuxStandalone64")
                return NamedBuildTarget.Standalone;
            if (platform == "Android")
                return NamedBuildTarget.Android;
            if (platform == "iOS")
                return NamedBuildTarget.iOS;
            if (platform == "WSA")
                return NamedBuildTarget.WindowsStoreApps;
            if (platform == "WebGL")
                return NamedBuildTarget.WebGL;
            if (platform == "tvOS")
                return NamedBuildTarget.tvOS;
            if (platform == "Stadia")
                return NamedBuildTarget.Stadia;
            if (platform == "PS4")
                return NamedBuildTarget.PS4;
            if (platform == "XboxOne" || platform == "GameCoreScarlett" || platform == "GameCoreXboxOne")
                return NamedBuildTarget.XboxOne;
            if (platform == "Switch")
                return NamedBuildTarget.NintendoSwitch;
            if (platform == "EmbeddedLinux")
                return NamedBuildTarget.EmbeddedLinux;

            return null;
        }

        private static NamedBuildTarget[] AllBuildTargets()
        {
            return new NamedBuildTarget[]
            {
                NamedBuildTarget.Standalone,
                NamedBuildTarget.Android,
                NamedBuildTarget.iOS,
                NamedBuildTarget.WindowsStoreApps,
                NamedBuildTarget.WebGL,
                NamedBuildTarget.tvOS,
                NamedBuildTarget.Stadia,
                NamedBuildTarget.PS4,
                NamedBuildTarget.XboxOne,
                NamedBuildTarget.NintendoSwitch,
                NamedBuildTarget.EmbeddedLinux,
                NamedBuildTarget.LinuxHeadlessSimulation,
                NamedBuildTarget.Server,
            };
        }
    }
}
