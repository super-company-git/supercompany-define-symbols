# Supercompany Define Symbols

Custom scripting define symbols system to be used by Unity packages.

## Description

Unity supports [Version Defines](https://docs.unity3d.com/Manual/ScriptCompilationAssemblyDefinitionFiles.html#define-symbols) in Assembly Definition files to allow setting a define symbol based on whether a package is present in the project or not. Unfortunately, this system does not support setting defines on a per-platform basis. So if you import a package that doesn't support, say WebGL games, and you have a define symbol to set whether the package is present in the project or not, the define symbol will be set even if you are in an unsupported platform.

This becomes even more problematic in the case of having a package that contains an optional dependency to another package.

**Supercompany Define Symbols** was created to solve this issue. With this package, define symbols are added automatically to the Player Settings of each platform, using the supported platforms set on an Assembly Definition file.

## Installing

First, install [GitDependencyResolverForUnity](https://github.com/mob-sakai/GitDependencyResolverForUnity) in your project.

Then install this package via [Unity Package Manager](https://docs.unity3d.com/Manual/upm-ui-giturl.html) using the following URL:

```
https://gitlab.com/super-company-git/supercompany-define-symbols.git?path=/Packages/io.supercompany.definesymbols#1.0.0-preview1
```

## How to use

1. Create an [Assembly Definition (asmdef) file](https://docs.unity3d.com/Manual/ScriptCompilationAssemblyDefinitionFiles.html). Set the supported platforms for such Assembly Definition. You **must** set the Editor as a supported platform.
2. In the same folder (or any sub-folders) that you created the Assembly Definition file, create an `AssemblyInfo.cs` script file (you can name it however you want). Delete everything inside this file and add the lines as the reference below:
```
[assembly: AssemblyAddDefine("MYCOMPANY_MYDEFINESYMBOL1")]
[assembly: AssemblyAddDefine("MYCOMPANY_MYDEFINESYMBOL2")]
[assembly: AssemblyDefinePrefix("MYCOMPANY_")]
```
3. You **must** be sure to use a prefix for all of your defines. Otherwise, if you delete an `AssemblyInfo.cs` file, or change the supported platforms on an Assembly Definition file, the define symbols will not be removed from the project.
4. You can use as many defines and prefixes as you want! You can also use one `AssemblyInfo.cs` for different Assembly Definition files.
5. Enjoy 🍾

## Limitations

You **must** be sure to use a prefix for all of your defines. Otherwise, if you delete an `AssemblyInfo.cs` file, or change the supported platforms on an Assembly Definition file, the define symbols will not be removed from the project.

For Assemblies with custom Define Symbols, you **must** set the Editor as a supported platform. Otherwise, the `AssemblyInfo.cs` will not be built on the Editor, so define symbols won't be found.
